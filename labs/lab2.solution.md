# Lab 2

## Solution

### Step 1: Git Branching Concepts

Branching allows developers to work on different features or bug fixes independently without affecting the main codebase. It enables parallel development, making collaboration more efficient and enabling experimentation without compromising the main branch's stability.


To list all local branches:
```bash
git branch --list
```

To list branches along with their last commit messages:
```bash
git branch -v
```

To list branches along with their remote-tracking branches:
```bash
git branch -vv
```

To list all branches, including remote branches:
```bash
git branch -a
```

### Step 2: Creating and Switching Branches

To create a new branch and switch to it:
```bash
git switch -c new_branch_name
```

To create a new branch and switch to it, even if the branch name already exists (will overwrite):
```bash
git switch -C existing_branch_name
```

Starting from Git version 2.23, git switch is recommended for branch switching as it has a more straightforward and safer behavior compared to git checkout.

### Step 3: Working with Files and Commits

To discard changes in a specific file and restore it to the last committed version:
```bash
git restore file_name
```

To stage all modified and deleted files and commit with a message:
```bash
git commit -a -m "Your commit message"
```

The -v option displays the diff of the changes being committed in the commit message editor. It helps you review your changes before finalizing the commit.

### Step 4: Branch Renaming and Merging

To rename the current branch:
```bash
git branch -m new_branch_name
```

To rename a different branch:
```bash
git branch -m old_branch_name new_branch_name
```

Merging combines changes from one branch into another. The following command will merge feature_branch into the current branch:

```bash
git merge feature_branch
```

### Step 5: Deleting Branches

To delete a fully merged branch:
```bash
git branch -d branch_to_delete
```

To delete a remote branch, use the following command:

```bash
git push origin --delete remote_branch
```
Replace remote_branch with the name of the remote branch you want to delete.

## Conclusion
You've now learned essential branching and merging concepts in Git. Branches
allow for parallel development, making collaboration more efficient. You've
also explored how to create, list, switch, rename, and delete branches, as well
as how to work with files and commits effectively. Understanding these concepts
will enable you to manage complex projects and collaborate with other
developers seamlessly.
