I've learned that there is a difference between `git checkout` and `git switch` and there is a reason to prefer the `switch` option.
I also learned that `git log` can draw a nice graph.
Now I understand that difference between upstream and origin.
Overall nice course and since I have some experience with git, I can better catch up with the lectures tempo, becouse as a newcomer I could be a little lost.
